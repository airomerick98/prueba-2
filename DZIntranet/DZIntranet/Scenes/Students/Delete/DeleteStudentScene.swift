//
//  DeleteStudentScene.swift
//  DZIntranet
//
//  Created by Kenyi Rodriguez on 21/04/22.
//

import Foundation

class DeleteStudentScene: Scene {
    
    var inputNumberDocument = DeleteStudentSceneInputs()
    var data: StudentsDataSourceProtocol!
    
    override func drawView() {
        super.drawView()
        self.getSearchKey()
    }
    
    private func getSearchKey() {
        let searchNumber = self.inputNumberDocument.txtSearch.getInput();
        guard let student = Students.shared.searchByKey(searchNumber) else {
            print("\nNo se encontraron resultado para: \(searchNumber)")
            _ = readLine()
            self.getSearchKey()
            return
        }
        self.showDeleteStudent(student)
    }
    
    private func showDeleteStudent(_ student: Student) {
        let indexStudent = Students.shared.indexStudent(student.document.number)
        var students = Students.shared.getAll()
        students.remove(at: indexStudent)
        print(students)
        print("Alumno Eliminado")
        self.backScene()
    }
}

//
//  DeleteStudentInput.swift
//  DZIntranet
//
//  Created by iMac on 30/04/22.
//

import Foundation

struct DeleteStudentSceneInputs {
    lazy var txtSearch: Input.Text = {
        
        let message = """
        Ingrese el número de documento por cual usted desea buscar al alumno
        """
        
        return Input.Text(message: message,
                   errorMessage: "El número de documento es incorrecto",
                   minLength: 1)
    }()
}
